import React from 'react'
import styled from 'styled-components';
import tw from 'twin.macro';
import Footer from '../../components/footer';

const FooterSectionContainer = styled.div`
    margin-top: 6em;
    ${tw`
        w-full
        pl-3
        pr-3
        lg:pl-12
        lg:pr-12
    `}
`;

export function FooterSection() {
    return (
        <FooterSectionContainer>
            <Footer />
        </FooterSectionContainer>
        
    )
}
