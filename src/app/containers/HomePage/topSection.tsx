import React from 'react'
import styled from 'styled-components';
import { Button } from '../../components/button';
import FemaleIllustration from "../../../assets/images/femaleworkoutillustration.jpg"
import { SCREENS } from '../../components/responsive';
import tw from 'twin.macro';

const TopSectionContainer = styled.div`
    margin-top: 6em;
    height: 80vh;
    ${tw`
        flex
        justify-between
        w-full
        max-w-screen-2xl
        pl-3
        pr-3
        lg:pl-12
        lg:pr-12
    `}
`;

const LeftContainer = styled.div`
    ${tw`
        w-1/2
        flex
        flex-col
    `}
`;

const RightContainer = styled.div`
    ${tw`
        w-1/2
        flex
        flex-col
        relative
        mt-20 
    `}
`;

// Edit this component to fit the mobile screen.
const Slogan = styled.h1`
    color: #008080;
    
    ${tw`
        font-semibold
        text-6xl
        mb-4
        transition
        duration-200
        ease-in-out
        hover:text-black
        sm:text-3xl
        sm:leading-snug
        md:text-5xl
        md:font-extrabold
        lg:font-black 
        lg:leading-normal
        duration-300
        xl:text-6xl 
        xl:leading-relaxed
        duration-700
    `}
`;

// Edit this component to fit the mobile screen.
const Description = styled.p` 

    ${tw`
        text-xs
        max-h-12
        text-blue-800
        sm:max-h-full
        lg:text-sm
        xl:text-lg
    `}
`;

// Edit this component to fit the mobile screen.
const FemaleWorkoutIllustration = styled.div`
    width: auto;
    height: 10em;
    right: -0.5em;
    top: -5em;
    position: absolute;

    img {
        width: auto;
        height: 100%;
        max-width: fit-content;
    }

    @media (min-width: ${SCREENS.sm}) {
        height: 16em;
        right: 8em;
        top: -6em;
    }

    @media (min-width: ${SCREENS.lg}) {
        height: 16 em;
        right: -4em;
        top: -6em;
    }

    @media (min-width: ${SCREENS.xl}) {
        height: 26em;
        right: 2em;
        top: -6em;
    }
`;

const ButtonsContainer = styled.div`
    ${tw`
        flex
        flex-wrap
        mt-6
        px-5
    `}
`;

export function TopSection() {
    return (
        <TopSectionContainer>
            <LeftContainer>
                <Slogan>Happy Friday!</Slogan>

                <Description>
                    I encourage you to mess around and change things within this workspace.
                </Description>

                <ButtonsContainer>
                    <Button theme="filled" text="Cole Button!" />
                    <Button theme="outlined" text="Cole New Button!" />
                    
                 
                </ButtonsContainer>
                
                
                   <p id='alertText'> Don't press the red button, else the site would crash!!!</p>
                
                
            </LeftContainer>

            <RightContainer>
                <FemaleWorkoutIllustration>
                    <img src={FemaleIllustration} title="Designed by slidesgo" alt="Illustration of a female working out." />
                </FemaleWorkoutIllustration>
            </RightContainer>
        </TopSectionContainer>
    )
}
