import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro';
import { Navbar } from '../../components/navbar';
import { FooterSection } from './footerSection';
import { MiddleSection } from './middleSection';
import { MiddleSection2 } from './middleSection2';
import { TopSection } from './topSection';

const PageContainer = styled.div`
    ${tw`
        flex
        flex-col
        w-full
        h-full
        items-center
        overflow-x-hidden
    `}
`;

export default function HomePage() {
    return (

        <PageContainer>
            <Navbar />
            <TopSection />
            <MiddleSection />
            <MiddleSection2 />
            <FooterSection />
        </PageContainer>
    )
}
