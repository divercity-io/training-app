import React from 'react'
import styled from 'styled-components';
import tw from 'twin.macro';
import Pricing from '../../components/pricing';

const MiddleSectionContainer = styled.div`

    margin-top: 6em;

    ${tw`

        flex
        justify-between
        w-full
        max-w-screen-2xl
        pl-3
        pr-3

        lg:pl-12
        lg:pr-12

    `}
`;

export function MiddleSection() {
    return (
        <MiddleSectionContainer>
            <Pricing />
        </MiddleSectionContainer>
        
    )
}
