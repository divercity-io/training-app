import React from 'react'
import styled, { css } from 'styled-components';
import tw from 'twin.macro';
import { slide as Menu } from 'react-burger-menu'
import { useMediaQuery } from 'react-responsive'
import { SCREENS } from '../responsive';
import menuStyles from './menuStyle'

const ListContainer = styled.ul`
    ${tw`
        flex
        list-none
    `}
`;

const NavItem = styled.li<{menu?: any}>`z
    ${tw`
        text-sm
        md:text-base
        text-black
        font-medium
        mr-1
        md:mr-12
        cursor-pointer
        transition
        duration-200
        ease-in-out
        hover:text-purple-800
        p-2
    `}

    ${({ menu }) => menu && css`
        ${tw`
            text-white
            text-xl
            mb-3
            focus:text-white
        `}
    `}
`;

const SignInItem = styled.li`
    ${tw`
        text-sm
        md:text-base
        text-black
        font-medium
        mr-1
        md:mr-12
        cursor-pointer
        transition
        duration-200
        ease-in-out
        hover:text-black
        text-blue-800
        p-2
    `}
`;

const SignUpItem = styled.li`
    ${tw`
        text-sm
        md:text-base
        text-black
        font-medium
        mr-1
        md:mr-12
        cursor-pointer
        transition
        duration-200
        ease-in-out
        hover:text-black
        text-blue-800
        border-solid
        rounded-md
        border-black
        border-2
        p-2
    `}
`;

export function NavItems() {

    const isMobile = useMediaQuery({ maxWidth: SCREENS.sm });

    if(isMobile) {
        return (
            <Menu right styles={menuStyles}>
                <ListContainer>
                    <NavItem menu>
                        <a href="#">Home</a>
                    </NavItem>

                    <NavItem menu>
                        <a href="#">Pricing</a>
                    </NavItem>

                    <NavItem menu>
                        <a href="#">What We Do</a>
                    </NavItem>

                    <NavItem menu>
                        <a href="#">Contact</a>
                    </NavItem>

                    <NavItem menu>
                        <a href="#">Login</a>
                    </NavItem>
                </ListContainer>
            </Menu>
        )
    };

    return (
        <ListContainer>
            <NavItem>
                <a href="#">Home</a>
            </NavItem>

            <NavItem>
                <a href="#">Pricing</a>
            </NavItem>

            <NavItem>
                <a href="#">What We Do</a>
            </NavItem>

            <NavItem>
                <a href="#">Contact</a>
            </NavItem>

            <SignInItem>
                <a href="#">Sign in</a>
            </SignInItem>

            <SignUpItem>
                <a href="#">Sign up</a>
            </SignUpItem>
        </ListContainer>
    )
}
