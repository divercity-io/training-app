export default { 
    bmBurgerButton: {
        position: 'fixed',
        width: '26px',
        height: '20px',
        right: '20px',
        top: '26px'
    },

    bmBurgerBars: {
        background: '#2c3e50'
    },

    bmBurgerBarsHover: {
        background: '#a90000'
    },

    bmCrossButton: {
        height: '24px',
        width: '24px'
    },

    bmCross: {
        background: '#f5f6fa'
    },

    bmMenuWrap: {
        position: 'fixed',
        top: '0',
        height: '100%'
    },

    bmMenu: {
        background: '#353b48',
        padding: '2.5em 1.5em 0',
        fontSize: '1.15em'
    },

    bmMorphShape: {
        fill: '#373a47'
    },

    bmItemList: {
        color: '#b8b7ad',
        padding: '0.8em'
    },

    bmItem: {
        display: 'inline-block'
    },

    bmOverlay: {
        background: 'rgba(0, 0, 0, 0.3)'
    },
};

  