import React, { Fragment } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro';
import { Logo } from '../../components/logo';
import { NavItems } from './navitems';
import { Disclosure, Menu, Transition } from '@headlessui/react'
import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline'

const NavbarContainer = styled.div`
    min-height:68px;

    ${tw`
        w-full
        min-w-full
        max-w-screen-2xl
        flex
        flex-row
        items-center
        lg:pl-12
        lg:pr-12 
        justify-between
    `}
`;

const LogoContainer = styled.div``;

/* const navigation = [
  { name: 'Dashboard', href: '#', current: true },
  { name: 'Team', href: '#', current: false },
  { name: 'Projects', href: '#', current: false },
  { name: 'Calendar', href: '#', current: false },
]

function classNames(...classes: string[]) {
    return classes.filter(Boolean).join(' ')
  }

*/

export function Navbar() {
    return (

        <NavbarContainer>
            <LogoContainer>
                <Logo />
            </LogoContainer>
            <NavItems />
        </NavbarContainer>

    // Paste TailwindUI > Navbar > <Simple with Menu Button on Left>

    )
}
