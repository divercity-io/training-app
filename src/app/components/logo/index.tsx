import React from 'react'
import styled from 'styled-components';
import tw from 'twin.macro';

import LogoImg from '../../../assets/images/projects.png';

const LogoContainer = styled.div`
    ${tw`
        flex
        items-center
        ml-12
    `}
`;

const LogoText = styled.div`
    ${tw`
        text-xl
        md:text-2xl
        font-bold
        text-purple-800
        m-1
    `}
`;

const Image = styled.div`

    width:auto;
    ${tw`
        h-6
        md:h-9
    `}

    img{
        width:auto;
        height:100%;
    }
`;


export function Logo() {
    return (
        <LogoContainer>
            <Image>
                <img src={LogoImg} alt="Image of a rocket ship" />
            </Image>
            <LogoText>Divercity Land</LogoText>
        </LogoContainer>
    )
}
