import React from 'react';
import styled from 'styled-components';
import tw from 'twin.macro';

interface IButtonProps {
    theme: "filled" | "outlined";
    text: string;
}

const BaseButton = styled.button`
    ${tw`
        pl-5
        pr-5 
        pt-3 
        pb-3 
        outline-none
        rounded-md
        text-white
        text-xs
        font-semibold
        border-solid
        border-transparent
        border-2 
        focus:outline-none 
        transition-all
        duration-200
        ease-in-out
        m-1 
    `}
`;

const OutlinedButton = styled(BaseButton)`
    ${tw`
        bg-red-500
        hover:bg-transparent
        hover:text-red-800
        hover:border-purple-800
    `}
`;

const FilledButton = styled(BaseButton)`
    ${tw`
        border-green-300
        text-white
        bg-green-700
        hover:bg-purple-800
        hover:text-white
        hover:border-transparent
        rounded-t-sm
    `}
`;

export function Button(props: IButtonProps) {

    const { theme, text } = props;

    if(theme === "filled") {
        return <FilledButton>{text}</FilledButton>
    } else {
        return <OutlinedButton>{text}</OutlinedButton>
    }
}