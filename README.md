# Welcome to the Divercity Coding Playground

## Important Documents 

What is Tailwind?

https://www.creative-tim.com/learning-lab/tailwind/nextjs/what-is-tailwind-css/notus

Learn how to use Tailwind!

https://tailwindcss.com/docs

CSS Colors

https://www.w3schools.com/cssref/pr_text_color.asp



### What are we going to do?

We will add components from the UI Kit from Tailwind.

Learn how to edit simple HTML and basic CSS.

Learn how React components work and how Tailwind interacts with those components.

### To Do

Make the topSection mobile responsive!

Edit the specified components within the topSection.tsx file. 





